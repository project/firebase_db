<?php

namespace Drupal\firebase_db\Controller;

use Drupal\Core\Controller\ControllerBase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Drupal\Component\Render\FormattableMarkup;

class FireBaseDb extends ControllerBase {

    public function content() {
        $database = $this->fireConnect();
        $reference = $database->getReference('blog/posts');
        $results = $reference->getSnapshot()->getValue();
        // kint($results);

        $header = [
            'key' => t('Key'),
            'title' => t('Title'),
            'body' => t('Body'),
            'edit' => t('Edit'),
            'delete' => t('Delete'),
        ];

        // Initialize an empty array
        $output = array();
// Next, loop through the $results array
        foreach ($results as $key => $result) {
            $output[$key] = [
                'key' => $key, // 'userid' was the key used in the header
                'title' => $result['title'], // 'Username' was the key used in the header
                'body' => $result['body'], // 'email' was the key used in the header
                'edit' => array('data' => new FormattableMarkup('<a href=":link">@name</a>', [':link' => '/firebase/' . $key . '/edit',
                        '@name' => 'Edit'])
                ),
                'delete' => array('data' => new FormattableMarkup('<a href=":link">@name</a>', [':link' => '/firebase/' . $key . '/delete',
                        '@name' => 'Delete'])),
            ];
        }
        return [
            '#type' => 'table',
            '#prefix' => '<a href="/firebase/add" target="_blank">Add Post</a>',
            '#header' => $header,
            '#options' => $output,
            '#rows' => $output,
            '#empty' => t('No users found'),];
    }

    public function fireDelete($key, $connection) {
        $reference = $connection->getReference('blog/posts/' . $key);
        $reference->remove();
    }

    /**
     * Stablish connection
     */
    public function fireConnect() {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/firekey.json');

        $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->withDatabaseUri('https://firstdb-b1dc5.firebaseio.com')
                ->create();
        return $firebase->getDatabase();
    }

    /**
     * Insert record
     */
    public function fireInsert($data, $connection) {
        //////////////////insert//////
        $newPost = $connection
                ->getReference('blog/posts')
                ->push($data);
        //   [
//            'title' => 'Post title',
//            'body' => 'This should probably be longer.'
//        ]
        return $newPost;
    }

    /**
     * update record
     */
    public function fireUpdate($data, $key, $connection) {

        $newPost = $connection
                ->getReference('blog/posts/' . $key)
                ->set($data);
    }

    /**
     * one  record
     */
    public function getRecord($key, $connection) {
        $reference = $connection->getReference('blog/posts/' . $key);
        $results = $reference->getSnapshot()->getValue();
        return $results;
    }

}
