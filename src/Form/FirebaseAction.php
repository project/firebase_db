<?php

namespace Drupal\firebase_db\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormStateInterface;
use \Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class AddLocation.
 *
 * @package Drupal\firebase_db\Form
 */
class FirebaseAction extends FormBase {

    /**
     * Stores the configuration factory.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    private $database;

    /**
     * {@inheritdoc}
     *
     *   The database connection.
     */
    public function __construct() {
        $this->database = Database::getConnection('default');
    }

    /**
     * {@inheritdoc}.
     */
    public function getFormId() {
        return 'firebase_db';
    }

    /**
     * {@inheritdoc}.
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        // Retrieve an array which contains the path pieces.
        $current_path = \Drupal::service('path.current')->getPath();
        $path_args = explode('/', $current_path);

        $form['action'] = array(
            '#type' => 'hidden',
            //'#title' => $this->t("title"),
            '#required' => TRUE,
            '#attributes' => array('readonly' => 'readonly'),
            '#default_value' => $path_args[3] ? $path_args[3] : 'add',
        );

        if ($path_args[3]) {
            $key = $path_args[2];

            $firedb = new \Drupal\firebase_db\Controller\FireBaseDb();
            $connection = $firedb->fireConnect();
            $record = $firedb->getRecord($key, $connection);

            $form['key'] = array(
                '#type' => 'hidden',
                //'#title' => $this->t("title"),
                '#required' => TRUE,
                '#attributes' => array('readonly' => 'readonly'),
                '#default_value' => (count($record) > 0) ? $key : '',
            );
        }

        if ($path_args[3] == 'delete') {

            $form['delete'] = array(
                '#type' => 'submit',
                '#prefix' => '<a href="/firebase/list">cancel</a>',
                '#title' => $this->t("Delete"),
                '#default_value' => $this->t("Delete"),
            );


            return $form;
        }

//  array_unshift($required, 'Select Required Time-');
        $form['title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t("title"),
            '#required' => TRUE,
            '#default_value' => $record['title'] ? $record['title'] : '',
        );
        $form['body'] = array(
            '#type' => 'textarea',
            '#title' => $this->t("body"),
            '#required' => TRUE,
            '#default_value' => $record['body'] ? $record['body'] : '',
        );

        $form['submit'] = array(
            '#type' => 'submit',
            '#title' => $this->t("Submit"),
            '#default_value' => $this->t("Submit"),
        );


        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state) {
        
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $values = $form_state->getValues();
        $firedb = new \Drupal\firebase_db\Controller\FireBaseDb();
        $connection = $firedb->fireConnect();

        $data = array('title' => $values['title'],
            'body' => $values['body']);


        if ($values['action'] == 'edit') {
            drupal_set_message('Record updated');
            $firedb->fireUpdate($data, $values['key'], $connection);
        } elseif ($values['action'] == 'add') {
            drupal_set_message('Record saved');
            $save = $firedb->fireInsert($data, $connection);
        } elseif ($values['action'] == 'delete') {
            drupal_set_message('Record delete');
            $firedb->fireDelete($values['key'], $connection);
        }
        $redirect = new RedirectResponse('/firebase/list');
        $redirect->send();
    }

}
