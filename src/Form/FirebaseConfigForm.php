<?php

/**
 * Save configurations
 *
 */

namespace Drupal\firebase_db\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FirebaseConfigForm
 * @package firebase
 */
class FirebaseConfigForm extends ConfigFormBase{
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'firebase_conf.token',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'firebase_conf_token';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Quiz Api Token for 14-16 Age Group'),
      '#default_value' => $this->config('quiz.token')->get('token'),
    ];
    $form['events_city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Events City Api Url'),
      '#default_value' => $this->config('quiz.token')->get('events_city'),
    ];
    $form['events_data'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Events Data Api Url'),
      '#default_value' => $this->config('quiz.token')->get('events_data'),
    ];
    $form['events_detail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Events Detail Data Api Url'),
      '#default_value' => $this->config('quiz.token')->get('events_detail'),
    ];
    $form['aviva_login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Aviva Login api url'),
      '#default_value' => $this->config('quiz.token')->get('aviva_login'),
    ];
    $form['aviva_email_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check to enable Hard Coded Email Settings to send birthday emails to users.'),
      '#default_value' => $this->config('quiz.token')->get('aviva_email_check'),
    ];
    $form['aviva_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter the Email ID where to send test emails.'),
      '#default_value' => $this->config('quiz.token')->get('aviva_email'),
    ];
    return parent::buildForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Store Token.
    $this->config('firebase_conf.token')->set('token', $form_state->getValue('token'))
        ->set('events_city', $form_state->getValue('events_city'))
        ->set('events_data', $form_state->getValue('events_data'))
        ->set('events_detail', $form_state->getValue('events_detail'))
        ->set('aviva_login', $form_state->getValue('aviva_login'))
        ->set('aviva_email_check', $form_state->getValue('aviva_email_check'))
        ->set('aviva_email', $form_state->getValue('aviva_email'))->save();
    drupal_set_message($this->t("Api's Data Saved."));
  }
}
?>
